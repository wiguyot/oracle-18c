#!/bin/bash

echo "CREATE TABLESPACE tbs_users DATAFILE '/opt/oradata/tbs_users01.dbf' SIZE 4096M EXTENT MANAGEMENT LOCAL AUTOALLOCATE ;"
echo "alter session set \"_ORACLE_SCRIPT\"=true;"
echo ""
echo ""

while read ligne
do
echo "DROP USER $ligne CASCADE ;"
echo "CREATE USER $ligne IDENTIFIED BY tutututu DEFAULT TABLESPACE tbs_users quota 10M on tbs_users temporary tablespace temp ;"
echo ""
echo "grant connect, resource, create view to $ligne ;"
echo ""
done < users-2019-09-17-v2.txt