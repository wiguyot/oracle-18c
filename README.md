# ORACLE 18c

Le présent dépôt a pour but de fournir
- un playbook ansible de déploiement d'Oracle 18c : playbook.yaml
- le fichier de hosts 
- un script de création des utilisateurs oracle
- quelques notes d'exploitation
- le playbook ansible qui a servi au déploiement de la base oracle de test
- le fichier vagrant qui permet de déployer un VM en local

## Prérequis d'usage du playbook ansible

- il faut que la MV cible soit dans le hosts...
- il faut que le $HOSTNAME de la machine à installer soit dans le DNS. Pour se faire nous avons fixé l'adresse MAC de notre machine, qui correspond à une réservation DHCP et un enregistrement dans le DNS.
- le fichier rpm d'installation d'Oracle **oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm** est soumis à licence. Il n'est donc pas fournit dans le dépôt. Pour le récupérer il est possible de créer un compte chez Oracle et de récupérer le fichier. 

## fonctionnement du playbook

- le playbook utilise l'utilisateur **limosadm** ayant des droits de sudoer sans mot de passe.
- les variables nom_DNS et adresse_IP sont très importantes car Oracle s'attend à exister dans le DNS. Il est important également que le localhost pointe bien sur l'adresse IP et non pas sur le loopback afin que le listener d'Oracle soit bien à l'écoute des demandes externes.
- les variables d'environnement sont positionnées : 
  - ORACLE_BASE=/opt/oracle
  - ORACLE_HOME=/opt/oracle/product/18c/dbhome_1
  - ORACLE_SID=ORCLCDB
- en plus d'Oracle sont installés 
  - vim
  - snmp
  - zabbix agent
  - un renvoi des logs vers le serveur de logs : 192.168.100.56
  - un lien symbolique de sqlplus dans /usr/bin/
- les ports réseaux suivants sont ouverts : 
  - 1521/TCP pour le listener d'Oracle
  - 161/UDP pour snmp
  - 10050-10051 pour zabbix

## Exploitation


- le démarrage de la base (et de son listener) se fait par **/etc/init.d/oracledb_ORCLCDB-18c start**
- c'est inscrit dans la crontab via **@reboot /root/oracle-start-at-boot.sh** qui lance l'instruction précédente.
- les enseignants ont accés au compte oracle (cf Marie Pailloux) qui n'est pas sudoer.
- les utilisateurs se connectent en ssh sur la machine en utilisant les comptes id/password => user01/user01 à user130/user130
- quand on veut intervenir sur la base oracle il ne faut pas oublier de passer par le compte oracle une fois qu'on est connecté sur la machine : 
```bash
sudo su oracle -
sqlplus / as sysdba
```
- le script **create-oracle-user.sh** permet de produire sur la sortie standard les instructions pour :
  - créer le table space
  - activer l'astuce pour permettre la création des utilisateur
  - les instructions de suppression du compte et sa (re)création, avec les grant qui vont bien.
 Bien entendu il faut exécuter ce script en étant connecté à la base en tant qu'oracle. Cf le point précédent. Attention le script commence par effacer le compte puis le crée. Il peut être souhaitable de ne pas faire de suppression d'utilisateur lors de la création... A voir en fonction du context.




