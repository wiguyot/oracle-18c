## utilisateur de test

CREATE TABLESPACE tbs_users DATAFILE '/opt/oradata/tbs_users01.dbf' SIZE 4096M EXTENT MANAGEMENT LOCAL AUTOALLOCATE ;

alter session set "_ORACLE_SCRIPT"=true;

CREATE USER utilisateur3 IDENTIFIED BY tutututu DEFAULT TABLESPACE tbs_users quota 1M on tbs_users temporary tablespace temp ;

grant connect, resource, create view to utilisateur2 ;

## utilisateur définitif

### création du tablespace

### création de l'utilisateur

alter session set "_ORACLE_SCRIPT"=true;

create user xxx identified by xxx

default tablespace yyy

temporary tablespace zzz

quota 1M on yyy

password expire ;


grant connect, resource, create view to xxx ;


## mise en place de script

comme utilisateur oracle : 

```
echo @essai.sql | /usr/local/bin/sqlplus / as sysdba
```