# INSTALLATION ORACLE 18c sur base CentOS 7

## Références
- [vieille façon d'installer oracle](http://www.be-root.com/2011/01/21/installation-doracle-11gr2-sur-centos-5/)
- [doc Oracle 18c version install RPM](https://docs.oracle.com/en/database/oracle/oracle-database/18/ladbi/running-rpm-packages-to-install-oracle-database.html)
- [doc générale d'installation Oracle 18c](https://docs.oracle.com/en/database/oracle/oracle-database/18/install-and-upgrade.html)

## On aide Oracle à placer ses petits en plaçant à la fin de /etc/profile


```bash
export ORACLE_BASE="/opt/oracle"
export ORACLE_HOME="$ORACLE_BASE/product/18c.0.3/db_1"
```

## Essentiel sinon aucune configuration possible de la BD à la fin de l'installation

**Editer le fichier /etc/hosts pour que l'hôte local ait une adresse IP**. Par exemple pour la machine **wgl-test-oracle18c**

```
172.16.36.107	wgl-test-oracle18c	localhost
```


## quelques paquets plus ou moins importants et mises à jour

```bash
sudo yum install -y epel-release
sudo yum -y update
sudo yum -y install vim htop wget git unzip
sudo yum -y install binutils compat-libstdc++-33 elfutils-libelf elfutils-libelf-devel gcc gcc-c++ glibc glibc-common glibc-devel glibc-headers kernel-headers libaio libaio-devel libgomp libstdc++ libstdc++-devel make numactl sysstat unixODBC-devel ksh
sudo yum install -y compat-libcap1 smartmontools
```


## Cœur de l'installation

Attention on considère que l'on a le fichier oracle-database-ee-18c-1.0-1.x86_64.rpm qui se télécharge via oracle et un compte actif (même gratuit) chez Oracle.

[Lien vers le fichier rpm](https://www.oracle.com/technetwork/database/enterprise-edition/downloads/oracle18c-linux-180000-5022980.html)


```bash
curl -o oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
sudo yum -y localinstall oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
sudo yum -y localinstall oracle-database-ee-18c-1.0-1.x86_64.rpm
sudo /etc/init.d/oracledb_ORCLCDB-18c configure
```

## résultat de l'installation si tout s'est bien passé
bien des minutes plus tard :
```yaml
Création de la base de données terminée. Pour plus de détails, reportez-vous aux fichiers journaux (/opt/oracle/cfgtoollogs/dbca/ORCLCDB).
Informations sur la base de données :
Nom global de base de données :ORCLCDB
Identificateur système (SID) :ORCLCDB
Pour plus de détails, reportez-vous au fichier journal "/opt/oracle/cfgtoollogs/dbca/ORCLCDB/ORCLCDB.log".

Database configuration completed successfully. The passwords were auto generated, you must change them by connecting to the database using 'sqlplus / as sysdba' as the oracle user.
```

## résultat de la commande netstat pour l'adresse IP du serveur oracle

On a bien le listener actif.

```yaml
[oracle@wgl-test-oracle18c centos]$ netstat -atun | grep "172.16.36.107"
tcp        0      0 172.16.36.107:29124     172.16.36.107:1521      ESTABLISHED
```

## On ajoute l'ouverture des du FW

```bash
sudo firewall-cmd --permanent --add-port=1521/tcp
sudo firewall-cmd --reload
```

La commande firewall-cmd --list-all permet de vérifier :

```yaml
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0
  sources:
  services: ssh dhcpv6-client
  ports: 1521/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## démarrage de la base

```
/etc/init.d/oracledb_ORCLCDB-18c start
```

## arrêt de la base

```
/etc/init.d/oracledb_ORCLCDB-18c stop
```

## configuration de la base avec les informations standards

```
/etc/init.d/oracledb_ORCLCDB-18c configure
```

## définition de la variable ORACLE_HOME

```bash
export ORACLE_HOME="/opt/oracle/product/18c/dbhome_1"
```

## pour se connecter sur le compte oracle

Il faut soit mettre il faut nous transmettre la clé publique de la personne souhaitant s'y connecter [ici](mailto:william.guyot@uca.fr)

## Pour le reste il faut connaître Oracle

```yaml
The passwords were auto generated, you must change them by connecting to the database using 'sqlplus / as sysdba' as the oracle user.
```
